// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "WonderLandGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class WONDERLAND_API AWonderLandGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
