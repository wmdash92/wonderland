// Copyright Epic Games, Inc. All Rights Reserved.

#include "WonderLand.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, WonderLand, "WonderLand" );
